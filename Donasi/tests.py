from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from django.contrib.auth.models import User
from .views import *
from .models import Donasi

class donasi_app(TestCase):
    def test_donasi_url_exist(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_index_func(self):
        found = resolve('/donasi/')
        self.assertEqual(found.func, donasi)

    def test_donasi_using_template(self):
        response = Client().get('/donasi/')
        self.assertTemplateUsed(response, 'donasi.html')

    def test_Donasi_object_exist_in_template(self):
        donasi = Donasi(nama="abc", nomor=123, nominal=1000)
        donasi.save()
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)

class form_donasi_app(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='username', password='password')
        self.client.login(username='username', password='password')

    def test_form_donasi_url_exist(self):
        response = self.client.get("/donasi/form/")
        self.assertEqual(response.status_code, 200)

    def test_form_donasi_using_template(self):
        response = self.client.get("/donasi/form/")
        self.assertTemplateUsed(response, 'form.html')

    def test_donasi_index_func(self):
        found = resolve("/donasi/form/")
        self.assertEqual(found.func, tambah_donasi)

    def test_form_donasi_post(self):
        response = self.client.post("/donasi/form/", {"nama" : "abc", "nomor": 123, "nominal": 1000})
        self.assertEqual(response.status_code, 302)

    def test_form_donasi_post_zero_nominal(self):
        response = self.client.post("/donasi/form/", {"nama" : "abc", "nomor": 123, "nominal": 0})
        self.assertEqual(response.status_code, 302)

    def test_Donasi_model_create_new_object(self):
        donasi = Donasi(nama="abc", nomor = 123, nominal = 1000 )
        donasi.save()
        self.assertEqual(Donasi.objects.all().count(), 1)
