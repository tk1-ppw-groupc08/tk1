from django.shortcuts import render, redirect
from .models import Donasi, Form_Donasi
from django.db.models import Sum
from django.contrib.auth.decorators import login_required
from django.contrib import messages

def donasi(request):
    count = Donasi.objects.all().count()
    if( count > 0):
        nominals = Donasi.objects.aggregate(total_donasi = Sum('nominal'))
        return render(request, 'donasi.html', nominals)
    else:
        nominals = {"total_donasi" : 0 }
        return render(request,'donasi.html', nominals)

@login_required(login_url="main:login")
def tambah_donasi(request):
    if (request.method == "POST"):
        form = Form_Donasi(request.POST)
        if form.is_valid():
            form.save()
            return redirect("Donasi:donasi")
        else:
            messages.info(request, 'data yang anda masukan tidak valid')
            return redirect("Donasi:form")
    else:
        form = Form_Donasi()
        return render (request, "form.html", {"form":form})

