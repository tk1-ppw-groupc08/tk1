from django.db import models
from django.forms import ModelForm
from django.core.validators import MinValueValidator, RegexValidator


class Donasi (models.Model):
    alphawithspace = RegexValidator(r'^[a-zA-Z ]*$', 'Only alphabet and space characters are allowed.')
    numeric = RegexValidator(r'^[0-9]*$', 'Only number characters are allowed.')
    nama = models.CharField("Nama Donatur", max_length=100, validators=[alphawithspace], help_text="*Nama lengkap atau nama panggilan *contoh: budi budiman atau budi.")
    nomor = models.CharField("Nomor Rekening", max_length=1000, validators=[numeric], help_text="*contoh: 0110xxx.")
    nominal = models.PositiveBigIntegerField("Nominal Donasi", validators=[MinValueValidator(1)], help_text="*contoh: 10000.")

class Form_Donasi (ModelForm):
    class Meta :
        model = Donasi
        fields = ["nama", "nomor", "nominal"]
# Create your models here.
