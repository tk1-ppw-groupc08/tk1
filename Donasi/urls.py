from django.urls import path
from .views import donasi, tambah_donasi

app_name = "Donasi"

urlpatterns = [
    path('', donasi, name="donasi"),
    path('form/', tambah_donasi, name='form')
]