from django.shortcuts import render, redirect
from .forms import UserRegistrationForm
from .models import Profile

def home(request):
    return render(request, 'main/home.html')

def register(request):
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            isTenagaMedis = form.cleaned_data.get('isTenagaMedis')
            Profile.objects.create(user=user, isTenagaMedis=isTenagaMedis)
            return redirect('main:login')
    else:
        form = UserRegistrationForm()
    return render(request, 'main/register.html', {'form':form})
