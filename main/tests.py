from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from .models import Profile
from django.contrib.auth.models import User


class MainTestCase(TestCase):
    #home
    def test_url_home_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = self.client.get(reverse('main:home'))
        self.assertTemplateUsed(response, "main/home.html")

    def test_content_home(self):
        response = self.client.get(reverse('main:home'))
        isi_template = response.content.decode('utf8')

        self.assertIn("SeputarCOVID", isi_template)
        self.assertIn("portal aplikasi seputar COVID-19", isi_template)
        self.assertIn("Daftar",isi_template)
        self.assertIn("Login", isi_template)
    
    #register
    def test_url_register(self):
        response = self.client.get(reverse('main:register'))
        self.assertEqual(response.status_code, 200)
    
    def test_template_register(self):
        response = self.client.get(reverse('main:register'))
        self.assertTemplateUsed(response, "main/register.html")

    def test_content_register(self):
        response = self.client.get(reverse('main:register'))
        isi_template = response.content.decode('utf8')

        self.assertIn("Buat akun baru", isi_template)
        self.assertIn("Username", isi_template)
        self.assertIn("Email",isi_template)
        self.assertIn("Password", isi_template)
        self.assertIn("Saya tenaga medis", isi_template)
        self.assertIn("Daftar", isi_template)
    
    def test_user_exist_after_registration(self):
        datauser = {
            'username': 'usertest',
            'email' : 'usertest@company.com',
            'password1': 'testingtesting123',
            'password2': 'testingtesting123',
            'isTenagaMedis' : False
        }
        response = self.client.post(reverse('main:register'), datauser)
        usertest = User.objects.get(username= "usertest")
        self.assertEqual(usertest.username, 'usertest')
        self.assertEqual(usertest.email, 'usertest@company.com')
        self.assertEqual(usertest.profile.isTenagaMedis, False)


    #login
    def test_url_login(self):
        response = self.client.get(reverse('main:login'))
        self.assertEqual(response.status_code, 200)
    
    def test_template_login(self):
        response = self.client.get(reverse('main:login'))
        self.assertTemplateUsed(response, "main/login.html")

    def test_content_login(self):
        response = self.client.get(reverse('main:login'))
        isi_template = response.content.decode('utf8')

        self.assertIn("Login", isi_template)
        self.assertIn("Username", isi_template)
        self.assertIn("Password", isi_template)
        self.assertIn("Belum punya akun?", isi_template)
        self.assertIn("Daftar", isi_template)

    #logout
    def test_url_logout(self):
        response = self.client.get(reverse('main:logout'))
        self.assertEqual(response.status_code, 200)

