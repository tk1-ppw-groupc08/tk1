from django.urls import path 
from . import views

app_name = 'artikel'

urlpatterns = [
    path('', views.home, name="home"), 
    path('add/', views.add, name="add"), 
    path('details/<int:article_id>/', views.details, name="details"),
    path('delete/<int:article_id>/', views.delete, name="delete"),
    path('edit/<int:article_id>/', views.edit, name="edit"),
]
