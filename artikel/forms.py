from django import forms 
from .models import Article 
from django.forms import widgets, TextInput, Textarea, DateTimeInput

class ArticleForm(forms.ModelForm):
    class Meta :
        model = Article
        fields = ['title', 'img', 'content']
        widgets = {
            'title' : TextInput(attrs={'placeholder' : 'Nama', 'required' : True}), 
            'img' : TextInput(attrs={'placeholder' : 'URL gambar, misal : https://picsum.photos/450/300?image=1072', 'required' : True}), 
            'content' : Textarea(attrs={'placeholder' : 'Konten', 'required' : True}),
            'time_posted': DateTimeInput(format='%d/%m/%Y %H:%M')
        }

