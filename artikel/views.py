from django.shortcuts import render, redirect 
from .models import Article 
from .forms import ArticleForm
from django.contrib.auth.decorators import login_required

def home(request):
    article = Article.objects.all()
    return render(request,"artikel/home.html", {'article' : article})

def details(request, article_id):
    obj = Article.objects.get(id=article_id)
    return render(request, 'artikel/details.html', {'obj' : obj})

@login_required
def add(request):
    form = ArticleForm(request.POST)
    if (form.is_valid() and request.method == 'POST' ) :
        user = request.user 
        instance = form.save(commit=False)
        instance.author_id = user.id 
        instance.save()
        return redirect('artikel:home')
    return render(request, 'artikel/add.html', {'form' : form, } )

@login_required
def delete(request, article_id):
    obj = Article.objects.get(id=article_id)
    obj.delete()
    return redirect('artikel:home')

@login_required
def edit(request, article_id):
    obj = Article.objects.get(id=article_id)
    form = ArticleForm(instance=obj)
    if request.method == "POST" :
        form = ArticleForm(request.POST, instance=obj)
        if form.is_valid() : 
            user = request.user 
            instance = form.save(commit=False)
            instance.author_id = user.id 
            instance.save()
            return redirect('artikel:home')
    return render(request, 'artikel/edit.html', {'form': form,})
   
        

