from django import forms
from .models import Laporan

class LaporForm(forms.Form):
    nama = forms.CharField(label='Nama', max_length=100)
    telp = forms.CharField(label='Nomor Telefon', max_length=100)
    prov = forms.CharField(label='Provinsi', max_length=100)
    kota = forms.CharField(label='Kabupaten/Kota', max_length=100)
