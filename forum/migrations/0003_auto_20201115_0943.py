# Generated by Django 3.1.2 on 2020-11-15 02:43

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0002_auto_20201115_0942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='date_posted',
            field=models.DateTimeField(default=datetime.datetime(2020, 11, 15, 9, 43, 46, 939493)),
        ),
        migrations.AlterField(
            model_name='threadreply',
            name='date_posted',
            field=models.DateTimeField(default=datetime.datetime(2020, 11, 15, 9, 43, 46, 940504)),
        ),
    ]
