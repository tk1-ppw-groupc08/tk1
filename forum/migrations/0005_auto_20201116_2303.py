# Generated by Django 3.1.2 on 2020-11-16 23:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0004_auto_20201115_0959'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thread',
            name='date_posted',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='threadreply',
            name='date_posted',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
