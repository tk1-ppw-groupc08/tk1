from django.test import TestCase, Client, RequestFactory
from django.urls import resolve
from .views import *
from .models import Thread, ThreadReply, ThreadForm, ThreadReplyForm
from django.contrib.auth.models import User, AnonymousUser
from django.utils import timezone, timesince


""" *snippet
self.user = User.objects.create_user(username='testuser', password='12345')
login = self.client.login(username='testuser', password='12345')
"""
class TestForumApp(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='jacob', email='jacob@xd.com', password='top_secret')
        self.thread = Thread(topic="abc", content="abc", user=self.user)
        self.thread.save()
        self.reply = ThreadReply.objects.create(
            content="xdxd", user=self.user, main_thread=self.thread)
        self.reply.save()
    

    """
    root **no login
    """
    def test_root_is_exist(self):
        response = Client().get('/forum/')
        self.assertEqual(response.status_code, 200)

    def test_root_views(self):
        found = resolve('/forum/')
        self.assertEqual(found.func, root)

    def test_root_use_template(self):
        response = Client().get('/forum/')
        self.assertTemplateUsed(response, 'forum/root.html')


    """
    thread **no login
    """
    def test_thread_is_exist(self):
        response = Client().get(f'/forum/{self.thread.id}/')
        self.assertEqual(response.status_code, 200)

    def test_thread_views(self):
        found = resolve(f'/forum/{self.thread.id}/')
        self.assertEqual(found.func, thread)

    def test_thread_use_template(self):
        response = Client().get(f'/forum/{self.thread.id}/')
        self.assertTemplateUsed(response, 'forum/topic.html')


    """
    add thread **login required
    """
    def test_add_with_login(self):
        request = self.factory.get('/forum/add/')
        request.user = self.user
        response = add_thread(request)
        self.assertEqual(response.status_code, 200)

    def test_add_without_login(self):
        request = self.factory.get('/forum/add/')
        request.user = AnonymousUser()
        response = add_thread(request)
        self.assertEqual(response.status_code, 302)

    def test_add_views(self):
        found = resolve('/forum/add/')
        self.assertEqual(found.func, add_thread)

    def test_add_use_template(self):
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.get('/forum/add/')
        self.assertTemplateUsed(response, 'forum/add_thread.html')


    """
    submit thread **login required
    """
    def test_submit_thread_with_login(self):
        data = {
            'topic' : 'rearea',
            'content' : 'rearea'
        }
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post('/forum/add/submit/', data=data)
        self.assertEqual(response.status_code, 302)


    """
    delete thread **login required
    """
    def test_delete_thread_with_login(self):
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post('/forum/1/delete/')
        self.assertEqual(response.status_code, 302)


    """
    add reply **login required
    """
    def test_add_reply_with_login(self):
        request = self.factory.get(f'/forum/{self.thread.id}/reply/')
        request.user = self.user
        response = add_thread(request)
        self.assertEqual(response.status_code, 200)

    def test_add_reply_without_login(self):
        response = Client().get(f'/forum/{self.thread.id}/reply/')
        self.assertEqual(response.status_code, 302)

    def test_add_reply_views(self):
        found = resolve(f'/forum/{self.thread.id}/reply/')
        self.assertEqual(found.func, add_reply)

    def test_add_reply_use_template(self):
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.get(f'/forum/{self.thread.id}/reply/')
        self.assertTemplateUsed(response, 'forum/add_reply.html')
    
    
    """
    submit reply **login required
    """
    def test_submit_reply_with_login(self):
        data = {
            'content' : 'rearea'
        }
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post(f'/forum/{self.thread.id}/reply/submit/', data=data)
        self.assertEqual(response.status_code, 302)


    """
    delete reply **login required
    """
    def test_delete_reply_with_login(self):
        reply = ThreadReply(content="rearea", user=self.user, main_thread=self.thread)
        reply.save()
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post('/forum/1/1/delete/')
        self.assertEqual(response.status_code, 302)

        
    """
    edit reply **login required
    """
    def test_edit_reply_with_login(self):
        request = self.factory.get(f'/forum/{self.thread.id}/{self.reply.id}/edit/')
        request.user = self.user
        response = edit_reply(request, self.thread.id, self.reply.id)
        self.assertEqual(response.status_code, 200)

    def test_edit_reply_without_login(self):
        response = Client().get(f'/forum/{self.thread.id}/{self.reply.id}/edit/')
        self.assertEqual(response.status_code, 302)

    def test_edit_reply_views(self):
        found = resolve(f'/forum/{self.thread.id}/{self.reply.id}/edit/')
        self.assertEqual(found.func, edit_reply)

    def test_edit_reply_use_template(self):
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.get(f'/forum/{self.thread.id}/{self.reply.id}/edit/')
        self.assertTemplateUsed(response, 'forum/add_reply.html')


    """
    submit edit reply **login required
    """
    def test_submit_edit_with_login(self):
        data = {
            'content' : 'rearea'
        }
        c = Client()
        c.login(username='jacob', password='top_secret')
        response = c.post(f'/forum/{self.thread.id}/{self.reply.id}/edit/submit/', data=data)
        self.assertEqual(response.status_code, 302)


class TestModel(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='jacob', email='jacob@xd.com', password='top_secret')
        self.thread = Thread(topic="abc", content="abc", user=self.user)
        self.thread.save()

    def test_thread_new_object(self):
        self.assertEqual(Thread.objects.all().count(), 1)

    def test_reply_new_object(self):
        reply = ThreadReply.objects.create(content="xdxd", user=self.user, main_thread=self.thread)
        reply.save()
        self.assertEqual(Thread.objects.all().count(), 1)
