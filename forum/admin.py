from django.contrib import admin
from .models import Thread, ThreadReply

admin.site.register(Thread)
admin.site.register(ThreadReply)