from django.shortcuts import render, redirect, reverse
from django.contrib.auth.decorators import login_required
from .models import Thread, ThreadReply, ThreadForm, ThreadReplyForm
from django.contrib.auth.models import User


def root(request):
    """
    basic forum views
    """
    threads = Thread.objects.all().order_by("-date_posted")
    context = {
        'threads' : threads
    }
    return render(request, 'forum/root.html', context)

@login_required(login_url='main:login')
def add_thread(request):
    """
    form views for adding new thread **login required
    """
    form = ThreadForm()
    context = {
        'form' : form
    }
    return render(request, 'forum/add_thread.html', context)

@login_required(login_url='main:login')
def submit_thread(request):
    """
    views for new thread submitting form **login required
    """
    if request.method == "POST":
        form = ThreadForm(request.POST)
        if form.is_valid():
            form.instance.user = request.user
            form.save()
    
    return redirect('forum:root')

def thread(request, thread_id):
    """
    views thread with specific topic
    """
    thread = Thread.objects.get(id=thread_id)
    replies = ThreadReply.objects.filter(main_thread=thread).all().order_by("-date_posted")
    context = {
        'thread' : thread,
        'replies' : replies
    }
    return render(request, 'forum/topic.html', context)

@login_required(login_url='main:login')
def delete_thread(request, thread_id):
    """
    delete thread (only if curent user login equal to thread author)
    """
    thread = Thread.objects.get(id=thread_id)
    if request.user == thread.user:
        thread.delete()
    return redirect('forum:root')

@login_required(login_url='main:login')
def add_reply(request, thread_id):
    """
    form views for adding reply to specific thread
    """
    form = ThreadReplyForm()
    context = {
        'form' : form,
        'thread' : Thread.objects.get(id=thread_id),
        'default' : "",
        'value' : "Jawab"
    }
    return render(request, 'forum/add_reply.html', context)

@login_required(login_url='main:login')
def submit_reply(request, thread_id):
    """
    views for submitting form
    """
    if request.method == "POST":
        form = ThreadReplyForm(request.POST)
        if form.is_valid():
            thread = Thread.objects.get(id=thread_id)
            form.instance.main_thread = thread
            form.instance.user = request.user
            form.save()

    return redirect(f'/forum/{thread_id}/')

@login_required(login_url='main:login')
def delete_reply(request, thread_id, reply_id):
    """
    views for delete reply
    """
    reply = ThreadReply.objects.get(id=reply_id)
    if request.user == reply.user:
        reply.delete()
    return redirect(f'/forum/{thread_id}')

@login_required(login_url='main:login')
def edit_reply(request, thread_id, reply_id):
    """
    form views for edit reply using add reply templates
    """
    reply = ThreadReply.objects.get(id=reply_id)
    content = reply.content
    form = ThreadReplyForm()
    context = {
        'default': content,
        'form': form,
        'thread' : Thread.objects.get(id=thread_id),
        'value' : "Edit"
    }
    return render(request, 'forum/add_reply.html', context)

@login_required(login_url='main:login')
def submit_edit(request, thread_id, reply_id):
    """
    submit edited reply
    """
    if request.method == "POST":
        form = ThreadReplyForm(request.POST)
        if form.is_valid():
            reply = ThreadReply.objects.get(id=reply_id)
            reply.content = form.instance.content
            reply.is_edited = True
            reply.save()

    return redirect(f'/forum/{thread_id}/')